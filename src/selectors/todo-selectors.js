import { createSelector } from "reselect";

export const getIsFetching = (state) => state.todos.isFetching;

export const getFilter = (state) => state.filter;

export const getTodoList = (state) => state.todos.todoList;

export const filteredTodo = createSelector(getFilter, getTodoList, (filter, todoList) => {
  return filter === 'completed' ? todoList.filter(todo => todo.completed) :
         filter === 'uncompleted' ? todoList.filter(todo => !todo.completed) : todoList
});