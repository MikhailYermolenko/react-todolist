import './App.css';
import Form from "./components/Form";
import TodoList from "./components/TodoList";
import React from "react";

const App = () => {
  return (
    <div className="App">
      <header>
        <h1>TODO list</h1>
      </header>
        <Form/>
        <TodoList/>
    </div>
  );
}

export default App;
