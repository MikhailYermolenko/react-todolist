import {
  ADD_TASK,
  COMPLETE_TASK,
  DELETE_TASK,
  CHANGE_FILTER,
  SET_NEW_TODO_LIST,
  RECEIPT_TODO_LIST_STARTED,
  RECEIPT_TODO_LIST_FINISH,
  RECEIPT_TODO_LIST_ERROR
} from '../constants'

export const addTodos = (todo) => ({ type: ADD_TASK, ...todo });

export const completeTask = (id) => ({ type: COMPLETE_TASK, id });

export const deleteTask = (id) => ({ type: DELETE_TASK, id });

export const changeFilter = (activeFilter) => ({ type: CHANGE_FILTER, activeFilter });

export const setNewTodoList = (newList) => ({ type: SET_NEW_TODO_LIST, newList });

export const fetchingError = (error) => ({ type: RECEIPT_TODO_LIST_ERROR, error })

export const getTodos = (id) => {
  return async(dispatch) => {
    try {
      dispatch({ type: RECEIPT_TODO_LIST_STARTED });
      const resp = await fetch(`https://jsonplaceholder.typicode.com/todos?userId=${id}`);
      const newList = await resp.json();
      dispatch(setNewTodoList(newList));
      dispatch({ type: RECEIPT_TODO_LIST_FINISH });
    }
    catch (e) {
      dispatch(fetchingError(e))
    }
  }
};