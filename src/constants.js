export const ADD_TASK = 'ADD_TASK';
export const COMPLETE_TASK = 'COMPLETE_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const CHANGE_FILTER = 'CHANGE_FILTER';
export const SET_NEW_TODO_LIST = 'SET_NEW_TODO_LIST';
export const RECEIPT_TODO_LIST_STARTED = 'RECEIPT_TODO_LIST_STARTED';
export const RECEIPT_TODO_LIST_FINISH = 'RECEIPT_TODO_LIST_FINISH';
export const RECEIPT_TODO_LIST_ERROR = 'RECEIPT_TODO_LIST_ERROR';
