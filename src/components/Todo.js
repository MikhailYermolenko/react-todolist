import React from 'react';
import { useDispatch } from 'react-redux';
import { completeTask, deleteTask } from "../actions/actionCreator"

const Todo = ({ todo, index }) => {
  const dispatch = useDispatch();
  const handleComplete = () => {
    console.log(todo);
    dispatch(completeTask(todo.id))
  };
  const handleDelete = () => {
    dispatch(deleteTask(todo.id))
  };

  return (
    <div className="todo">
      <li className={`todo-item ${todo.completed ? "completed" : ''}`}>{todo.title}</li>
      <button onClick={handleComplete} className="complete-btn">
        <i className="fas fa-check"/>
      </button>
      <button onClick={handleDelete} className="trash-btn">
        <i className="fas fa-trash"/>
      </button>
    </div>
  )
}

export default Todo;