import React, { useState } from 'react';
import { useDispatch, useSelector} from 'react-redux';
import { changeFilter, addTodos, getTodos } from "../actions/actionCreator";

const Form = () => {
  const [inputText, setInputText] = useState("");
  const dispatch = useDispatch();
  const filter = useSelector(state => state.filter)

  const inputTextHandler = (e) => {
    setInputText(e.target.value)
  };

  const handlerGetTodos = (e) => {
    e.preventDefault();
    dispatch(getTodos(1))
  };

  const submitTodoHandler = (e) => {
    e.preventDefault();

    if (inputText.length) {
      dispatch(addTodos({ id: Math.random() * 1000, title: inputText }));
    }
    setInputText('');
  };

    return (
      <form>
        <input onChange={inputTextHandler} value={inputText} type="text" className="todo-input"/>
        <button onClick={submitTodoHandler} className="todo-button" type="submit">
          <i className="fas fa-plus-square"/>
        </button>
        <div className="select">
          <select onChange={(e) => dispatch(changeFilter(e.target.value))} value={filter} className="filter-todo">
            <option value="all">All</option>
            <option value="completed">Comleted</option>
            <option value="uncompleted">Uncompleted</option>
          </select>
        </div>
        <button onClick={(e) => handlerGetTodos(e)} className="todo-button" type="submit">
          Get todolist
        </button>
      </form>
    )
}

export default Form;