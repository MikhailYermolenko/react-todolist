import React from 'react';
import { useSelector } from 'react-redux';

import Todo from "./Todo";
import Loader from "./Loader";
import { filteredTodo, getIsFetching } from "../selectors/todo-selectors";

const TodoList = () => {
  const todoList = useSelector(state => filteredTodo(state));
  const isFetching = useSelector(state => getIsFetching(state));

  let preloader;
  if(isFetching) preloader = <Loader/>;

  return (
      <div className="todo-container">
        {preloader}
        <ul className={`todo-list ${isFetching ? 'in-pending' : ''}`}>
          { todoList.map(todo => (
            <Todo
              todo={todo}
              key={todo.id}
            />
          ))}
        </ul>
      </div>
    )
}

export default TodoList;