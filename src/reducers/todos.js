import {
  ADD_TASK,
  COMPLETE_TASK,
  DELETE_TASK,
  SET_NEW_TODO_LIST,
  RECEIPT_TODO_LIST_FINISH,
  RECEIPT_TODO_LIST_STARTED,
  RECEIPT_TODO_LIST_ERROR
} from '../constants';

const initialState = {
  todoList: [{id: 123456, title: 'asfdgf', completed: false}],
  isFetching: false
};

const getIndex = (todoList, id) => todoList.findIndex(todo => todo.id === id);

const todos = (state = initialState, { id, title, type, newList, error }) => {
  switch (type) {
    case ADD_TASK:
      return {
        ...state,
        todoList: [...state.todoList, {
          id: id,
          title: title,
          completed: false
        }]
      };
    case COMPLETE_TASK:
      const completeIndex = getIndex(state.todoList, id);

      state.todoList[completeIndex].completed = !state.todoList[completeIndex].completed;
      return {
        ...state,
        todoList: [...state.todoList]
      };
    case DELETE_TASK:
      const deleteIndex = getIndex(state.todoList, id);

      state.splice(deleteIndex, 1)
      return {
        ...state,
        todoList: [...state.todoList]
      };
    case SET_NEW_TODO_LIST:
      return { ...state, todoList: [...state.todoList, ...newList] };
    case RECEIPT_TODO_LIST_STARTED:
      return {...state, isFetching: !state.isFetching};
    case RECEIPT_TODO_LIST_FINISH:
      return {...state, isFetching: !state.isFetching};
    case RECEIPT_TODO_LIST_ERROR:
      console.error(error);
      return {...state, isFetching: !state.isFetching}
    default:
      return state;
  }
};

export default todos;