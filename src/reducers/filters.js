import { CHANGE_FILTER } from '../constants';

const initialState = {
  filter: 'all'
};

const filter = (state = initialState.filter, { type, activeFilter }) => {
  switch (type) {
    case CHANGE_FILTER:
      return activeFilter;
    default:
      return state;
  }
}

export default filter;